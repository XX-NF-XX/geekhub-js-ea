const DEFAULT_WIDTH = 10;
const DEFAULT_HEIGHT = 24;

class Field {

    static get DEFAULT_HEIGHT() {
        return DEFAULT_HEIGHT;
    }

    static get DEFAULT_WIDTH() {
        return DEFAULT_WIDTH;
    }

    constructor(width = DEFAULT_WIDTH, height = DEFAULT_HEIGHT) {
        let field = new Array(height).fill(null).map(() => new Array(width).fill(0));

        this.getMatrix = function () {
            return getCopy(field);
        };

        this.apply = function (matrix, {x, y}) {
            field = this.map(matrix, {x, y}, (fieldCell, matrixCell) => {
                if (matrixCell !== 0) return matrixCell;
            });
        };

        this.removeFullRows = function() {
            const newField = field.filter((row) => {
                return row.some((cell) => {
                    return cell === 0;
                });
            });
    
            const removed = field.length - newField.length;
    
            for (let i = 0; i < removed; i++) {
                newField.unshift(new Array(width).fill(0));
            }

            field = newField;
    
            return removed;
        };
    }

    map(matrix, {x, y}, callback) {
        const field = this.getMatrix();

        for(let h = 0; h < matrix.length; h++) {
            for(let w = 0; w < matrix[h].length; w++) {

                const fieldY = h + y;
                const fieldX = w + x;
                
                if (!this.within({x: fieldX, y: fieldY})) continue;

                const result = callback(field[fieldY][fieldX], matrix[h][w]);
                if (typeof result !== 'undefined') field[fieldY][fieldX] = result;
            }
        }
        return field;
    }

    some(matrix, {x, y}, callback) {
        const field = this.getMatrix();

        for(let h = 0; h < matrix.length; h++) {
            for(let w = 0; w < matrix[h].length; w++) {

                const fieldY = h + y;
                const fieldX = w + x;

                if (!this.within({x: fieldX, y: fieldY})) continue;

                if (callback(field[fieldY][fieldX], matrix[h][w]) === true) return true;
            }
        }
        return false;
    }

    within({x, y, width = 1, height = 1}, ignoreTop = false) {
        const offset = this.getOffset({x, y, width, height});

        if (ignoreTop) return offset.x === 0 && offset.y <= 0;

        return offset.x === 0 && offset.y === 0;
    }

    getOffset({x, y, width, height}) {
        const field = this.getMatrix();

        let resultX = 0;
        let resultY = 0;

        if (x < 0) {
            resultX = x;
        } else if (x + width > field[0].length) {
            resultX = x + width - field[0].length;
        }

        if (y < 0) {
            resultY = y;
        } else if (y + height > field.length ) {
            resultY = y + height - field.length;
        }

        return {x: resultX, y: resultY};
    }

    hasFullRows() {
        const field = this.getMatrix();

        for (let h = 0; h < field.length; h++) {
            const isFilled = field[h].every((cell) => {
                return cell > 0;
            });

            if (isFilled) return true;
        }

        return false;
    }
}

function getCopy(field) {
    return field.slice().map((row) => row.slice());
}

export {Field};