import {Field} from './field.js';
import {Tetromino} from './tetromino.js';
import {GameState} from './gamestate.js';

const INITIAL_SPEED = 1000;
const FALLING_SPEED = 50;

class Game {
    constructor() {
        this.field = new Field();
        this.gameState = new GameState();

        this.fieldUpdateHandler = null;

        this.speed = INITIAL_SPEED;
        this.fallingSpeed = FALLING_SPEED;

        this.updateIntervalID = null;
        this.fallingIntervalID = null;

        this.nextTetromino = null;
        this.tetromino = null;
    }

    updateSpeed() {
        this.speed = INITIAL_SPEED * Math.pow(0.8, this.gameState.level - 1);
    }

    getFieldMatrix() {
        if (this.tetromino !== null) {
            return this.field.map(this.tetromino.getMatrix(), this.tetromino.position, (field, tetro) => {
                if (tetro !== 0) return tetro;
            });
        }
        return this.field.getMatrix();
    }

    getFieldSize() {
        return {
            width: Field.DEFAULT_WIDTH,
            height: Field.DEFAULT_HEIGHT,
        };
    }

    getInfo() {
        return {
            score: this.gameState.score,
            level: this.gameState.level,
            tetromino: this.nextTetromino.getMatrix(),
        };
    }

    start(fieldUpdateHandler, infoUpdateHandler) {
        this.fieldUpdateHandler = fieldUpdateHandler;

        this.gameState.initHandlers(infoUpdateHandler, () => this.updateSpeed());

        this.generateNextTetromino();

        this.startUpdating();
        this.update();
    }

    startUpdating() {
        if (this.updateIntervalID !== null) this.stopUpdating();

        this.updateIntervalID = setInterval(this.update.bind(this), this.speed);
    }

    update() {
        if (this.tetromino === null) this.addNewTetromino();

        this.step();

        if (this.fieldUpdateHandler) this.fieldUpdateHandler();
    }

    stopUpdating() {
        if (this.updateIntervalID === null) return;

        clearInterval(this.updateIntervalID);
        this.updateIntervalID = null;
    }

    addNewTetromino() {
        this.tetromino = this.nextTetromino;

        const offsetY = this.tetromino.getRelativeBounds().y;

        this.tetromino.position = {
            x: Math.floor((this.getFieldSize().width - this.tetromino.getRelativeBounds().width) / 2),
            y: -offsetY - 1,
        };
        this.generateNextTetromino();
    }

    generateNextTetromino() {
        this.nextTetromino = Tetromino.createRandomTetromino();
        this.gameState.updateInfo();
    }

    step() {
        const newPosition = this.tetromino.position;
        newPosition.y += 1;

        if (this.field.getOffset(this.tetromino.getRelativeBounds(newPosition)).y > 0) {
            this.addTetrominoToField();
            return;
        }

        if (this.isOverlapping(newPosition)) {
            this.addTetrominoToField();
            return;
        }

        this.tetromino.position = newPosition;
    }

    move({x = 0, y = 0}) {
        if (this.tetromino === null) return false;

        const newPosition = this.tetromino.position;
        newPosition.x += x;
        newPosition.y += y;

        if (!this.field.within(this.tetromino.getRelativeBounds(newPosition), true)) {
            return false;
        }

        if (this.isOverlapping(newPosition)) {
            return false;
        }

        this.tetromino.position = newPosition;

        if (this.fieldUpdateHandler) this.fieldUpdateHandler();

        return true;
    }

    startForcedFalling() {
        if (this.fallingIntervalID !== null || this.tetromino === null) return;

        this.stopUpdating();

        this.fallingIntervalID = setInterval(this.forceFall.bind(this), this.fallingSpeed);
    }

    forceFall() {
        if (this.tetromino === null) return;

        const isMoved = this.move({y: 1});

        if (!isMoved) {
            this.addTetrominoToField();
            this.update();
        } else {
            this.gameState.addDropScore();
        }
    }

    stopForcedFalling() {
        if (this.fallingIntervalID === null) return;

        clearInterval(this.fallingIntervalID);
        this.fallingIntervalID = null;

        this.startUpdating();
    }

    rotateTetromino(clockwise = true) {
        if (this.tetromino === null) return;

        this.tetromino.rotate(clockwise);
        
        const offsetX = this.field.getOffset(this.tetromino.getRelativeBounds()).x;

        const newPosition = this.tetromino.position;
        newPosition.x -= offsetX;

        if (this.isOverlapping(newPosition)) {
            this.tetromino.rotate(!clockwise); //Undo previously applied rotating
        } else {
            this.tetromino.position = newPosition;
            this.fieldUpdateHandler();
        }
    }

    addTetrominoToField() {
        this.field.apply(this.tetromino.getMatrix(), this.tetromino.position);
        this.tetromino = null;

        this.removeFullRows();
    }

    removeFullRows() {
        if (!this.field.hasFullRows()) return;

        this.stopUpdating();
        
        const removedRows = this.field.removeFullRows();
        this.gameState.addRowScore(removedRows);

        this.startUpdating();
    }

    isOverlapping(position) {
        return this.field.some(this.tetromino.getMatrix(), position, (field, tetro) => {
            return field !== 0 && tetro !== 0;
        });
    }
}

export {Game};
