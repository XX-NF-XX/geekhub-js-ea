const tetrominoTypes = Object.freeze({
    I: Symbol('TETROMINO_I'),
    L: Symbol('TETROMINO_L'),
    J: Symbol('TETROMINO_J'),
    T: Symbol('TETROMINO_T'),
    S: Symbol('TETROMINO_S'),
    Z: Symbol('TETROMINO_Z'),
    O: Symbol('TETROMINO_O')
});

class Tetromino {

    static get TYPE() {
        return tetrominoTypes;
    }

    static createRandomTetromino() {
        const types = Object.values(tetrominoTypes);
        const randomTypeIdx = Math.trunc(Math.random() * types.length);
        return new Tetromino(types[randomTypeIdx]);
    }

    constructor(typeOfTetromino) {
        if (!Object.values(tetrominoTypes).includes(typeOfTetromino)) {
            throw new TypeError('Cannot create Tetromino. Unknown type.');
        }

        const type = typeOfTetromino;
        let rotationType = 0;

        let matrix = getMatrix(type, rotationType);

        this.x = 0;
        this.y = 0;

        this.getType = function() {
            return type;
        };

        this.getMatrix = function() {
            return getCopy(matrix);
        };

        this.rotate = function(clockwise = true) {
            rotationType = clockwise ? rotationType - 1 : rotationType + 1;
            
            if (rotationType < 0) rotationType = 3;
            if (rotationType > 3) rotationType = 0;

            matrix = getMatrix(type, rotationType);
        };
    }

    getRelativeBounds(position = this.position) {
        let minY = Number.POSITIVE_INFINITY;
        let maxY = Number.NEGATIVE_INFINITY;
        let minX = Number.POSITIVE_INFINITY;
        let maxX = Number.NEGATIVE_INFINITY;

        const matrix = this.getMatrix();

        for(let h = 0; h < matrix.length; h++) {
            for(let w = 0; w < matrix[h].length; w++) {
                if (matrix[h][w] !== 0) {
                    if (minY > h) minY = h;
                    if (maxY < h) maxY = h;
                    if (minX > w) minX = w;
                    if (maxX < w) maxX = w;
                }
            }
        }

        return {
            x: minX + position.x,
            y: minY + position.y,
            width: maxX - minX + 1,
            height: maxY - minY + 1,
        };
    }

    get position() {
        return {x: this.x, y: this.y};
    }

    set position({x = this.x, y = this.y}) {
        this.x = x;
        this.y = y;
    }
}

function getMatrix(tetrominoType, rotationType) {
    switch (tetrominoType) {
    case tetrominoTypes.I:
        if (rotationType === 0 || rotationType === 2) return [[0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1], [0, 0, 0, 0]];
        if (rotationType === 1 || rotationType === 3) return [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]];
        break;
    case tetrominoTypes.O:
        if (rotationType >= 0 && rotationType <= 3) return [[1, 1], [1, 1]];
        break;
    case tetrominoTypes.L:
        if (rotationType === 0) return [[0, 0, 0], [1, 1, 1], [1, 0, 0]];
        if (rotationType === 1) return [[0, 1, 0], [0, 1, 0], [0, 1, 1]];
        if (rotationType === 2) return [[0, 0, 1], [1, 1, 1], [0, 0, 0]];
        if (rotationType === 3) return [[1, 1, 0], [0, 1, 0], [0, 1, 0]];
        break;
    case tetrominoTypes.J:
        if (rotationType === 0) return [[0, 0, 0], [1, 1, 1], [0, 0, 1]];
        if (rotationType === 1) return [[0, 1, 1], [0, 1, 0], [0, 1, 0]];
        if (rotationType === 2) return [[1, 0, 0], [1, 1, 1], [0, 0, 0]];
        if (rotationType === 3) return [[0, 1, 0], [0, 1, 0], [1, 1, 0]];
        break;
    case tetrominoTypes.S:
        if (rotationType === 0 || rotationType === 2) return [[0, 0, 0], [0, 1, 1], [1, 1, 0]];
        if (rotationType === 1 || rotationType === 3) return [[1, 0, 0], [1, 1, 0], [0, 1, 0]];
        break;
    case tetrominoTypes.Z:
        if (rotationType === 0 || rotationType === 2) return [[0, 0, 0], [1, 1, 0], [0, 1, 1]];
        if (rotationType === 1 || rotationType === 3) return [[0, 1, 0], [1, 1, 0], [1, 0, 0]];
        break;
    case tetrominoTypes.T:
        if (rotationType === 0) return [[0, 0, 0], [1, 1, 1], [0, 1, 0]];
        if (rotationType === 1) return [[0, 1, 0], [1, 1, 0], [0, 1, 0]];
        if (rotationType === 2) return [[0, 1, 0], [1, 1, 1], [0, 0, 0]];
        if (rotationType === 3) return [[0, 1, 0], [0, 1, 1], [0, 1, 0]];
        break;
    default:
        return null;
    }
}

function getCopy(matrix) {
    return matrix.slice().map((row) => row.slice());
}

export {Tetromino};