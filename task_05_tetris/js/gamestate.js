class GameState {
    constructor() {
        this.score = 0;
        this.level = 1;
        this.rowsRemoved = 0;
        this.infoUpdateHandler = null;
        this.levelChangeHandler = null;
    }

    addRowScore(rowsRemoved) {
        let rowScore = 0;

        if (rowsRemoved === 1) rowScore = 40;
        if (rowsRemoved === 2) rowScore = 100;
        if (rowsRemoved === 3) rowScore = 300;
        if (rowsRemoved === 4) rowScore = 1200;

        this.score += rowScore * this.level;
        this.rowsRemoved += rowsRemoved;

        this.updateLevel();

        this.updateInfo();
    }

    updateLevel() {
        const newLevel = Math.trunc(this.rowsRemoved / 10) + 1;

        if (this.level !== newLevel) {
            this.level = newLevel;
            
            if (this.levelChangeHandler !== null) this.levelChangeHandler();
        }
    }

    addDropScore() {
        this.score++;
        this.updateInfo();
    }

    initHandlers(infoUpdateHandler, levelChangeHandler) {
        this.infoUpdateHandler = infoUpdateHandler;
        this.levelChangeHandler = levelChangeHandler;
    }

    updateInfo() {
        if (this.infoUpdateHandler !== null) this.infoUpdateHandler();
    }
}

export {GameState};
