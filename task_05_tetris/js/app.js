import {Game} from './game.js';
import {UI} from './UI.js';
import {Input} from './input.js';

const game = new Game();
const ui = UI.instance;
const input = Input.instance;

input.onLeftHandler = () => game.move({x: -1});
input.onRightHandler = () => game.move({x: 1});
input.onDropPressHandler = () => game.startForcedFalling();
input.onDropUnpressHandler = () => game.stopForcedFalling();
input.onRotateHandler = () => game.rotateTetromino(true);

input.startListening();

ui.drawGame(game.getFieldSize());

game.start(() => ui.updateField(game.getFieldMatrix()), () => ui.updateInfo(game.getInfo()));
