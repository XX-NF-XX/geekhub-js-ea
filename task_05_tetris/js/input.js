let inputInstance = null;

const ARROW_LEFT = 37;
const ARROW_UP = 38;
const ARROW_RIGHT = 39;
const ARROW_DOWN = 40;
const SPACE = 32;

const REPEAT_INTERVAL = 100;

class Input {

    static get instance() {
        if (inputInstance !== null) return inputInstance;

        inputInstance = new Input();
        return inputInstance;
    }

    static set instance(argument) {
        throw new Error('Input singleton cannot be reassigned!');
    }

    constructor() {
        if (inputInstance instanceof Input) return inputInstance;

        this.onLeftHandler = null;
        this.onRightHandler = null;
        this.onDropPressHandler = null;
        this.onDropUnpressHandler = null;
        this.onRotateHandler = null;

        this.repeatIntervalID = null;
    }

    startListening() {
        document.addEventListener('keydown', this.keyHandle.bind(this));
        document.addEventListener('keyup', this.keyHandle.bind(this));
    }

    keyHandle(event) {
        if (event.repeat) return;
        
        if (event.type === 'keydown') {
            switch (event.keyCode) {
            case ARROW_LEFT:
                if (this.onLeftHandler) this.startRepeating(this.onLeftHandler);
                break;
            case ARROW_RIGHT:
                if (this.onRightHandler) this.startRepeating(this.onRightHandler);
                break;
            case ARROW_UP:
                if (this.onRotateHandler) this.onRotateHandler();
                break;
            case ARROW_DOWN: 
            case SPACE:
                if (this.onDropPressHandler) this.onDropPressHandler();
                break;
            }
        }

        if (event.type === 'keyup') {
            if (event.keyCode === ARROW_DOWN || event.keyCode === SPACE) {
                if (this.onDropUnpressHandler) this.onDropUnpressHandler();
            }

            if (event.keyCode === ARROW_LEFT || event.keyCode === ARROW_RIGHT) {
                this.stopRepeating();
            }
        }
    }

    startRepeating(callback) {
        this.stopRepeating();

        if (callback) {
            callback();
            this.repeatIntervalID = setInterval(callback, REPEAT_INTERVAL);
        }
    }

    stopRepeating() {
        if (this.repeatIntervalID !== null) {
            clearInterval(this.repeatIntervalID);
            this.repeatIntervalID = null;
        }
    }
}

export {Input};