let UIinstance = null;

const MAIN_CONTAINER_ID = 'main-container';
const FIELD_ID = 'field';
const INFO_ID = 'info';
const SCORE_ID = 'score';
const NEXT_TETROMINO_FIELD_ID = 'next-tetromino';
const LEVEL_ID = 'level';

const BODY_PADDING = 10;

const FONT_NAME = 'Courier New';
const FONT_SIZE = 5;

const FIELD_SIZE_WIDTH = 0.6; // 0.6 = 60% for field and 40% (the rest) for info panel
const FIELD_BORDER = '1px solid #CCC';
const NEXT_TETROMINO_FIELD_SIZE = 4; // 4x4
const NEXT_TETROMINO_FIELD_BORDER = '1px solid #FFF';

const WINDOW_SIZE_WIDTH_MIN = 66; //Pixels
const WINDOW_SIZE_HEIGHT_MIN = 88;

let isResizeRunning = false;

class UI { //Singleton

    static get instance() {
        if (UIinstance !== null) return UIinstance;

        UIinstance = new UI();
        return UIinstance;
    }

    static set instance(argument) {
        throw new Error('UI singleton cannot be reassigned!');
    }

    constructor() {
        if (UIinstance instanceof UI) return UIinstance;

        this.fieldWidth = null;
        this.fieldHeight = null;

        addBaseElements();
    }

    drawGame({width, height}) {
        if (this.fieldHeight !== null || this.fieldWidth !== null) {
            throw new Error('Game UI has already been drawn!');
        }
        if (!Number.isSafeInteger(width) || !Number.isSafeInteger(height)) {
            throw new Error('Parameter must be a safe integer!');
        }
        if (width < 1 ||  height < 1) {
            throw new Error('Parameter must be bigger than 0');
        }

        this.fieldWidth = width;
        this.fieldHeight = height;
        
        const sizes = getSizes(this.fieldWidth, this.fieldHeight);
        setMainContainerProperties(sizes);

        addField(this.fieldWidth, this.fieldHeight);
        addInfo();

        this.resizeGame();

        window.addEventListener('resize', onWindowResize);
    }

    resizeGame() {
        const sizes = getSizes(this.fieldWidth, this.fieldHeight);
        resizeContainer(sizes);
        resizeFont(sizes);
        resizeNextTetrominoTable(sizes);
    }

    updateField(fieldData) {
        updateTable(fieldData, FIELD_ID);
    }

    updateInfo({score, level, tetromino}) {
        updateInfo(score, level, tetromino);
    }
}

function getSizes(fieldWidth, fieldHeight) {
    const windowWidth = Math.max(window.innerWidth, WINDOW_SIZE_WIDTH_MIN);
    const windowHeight = Math.max(window.innerHeight, WINDOW_SIZE_HEIGHT_MIN);

    const maxCellWidth = (windowWidth - BODY_PADDING * 2) * FIELD_SIZE_WIDTH / fieldWidth;
    const maxCellHeight = (windowHeight - BODY_PADDING * 2) / fieldHeight;

    const cellSize = Math.min(maxCellWidth, maxCellHeight);

    const containerWidth = cellSize * fieldWidth / FIELD_SIZE_WIDTH;
    const containerHeight = cellSize * fieldHeight;

    const byWidth = cellSize === maxCellWidth;
    
    return { containerWidth, containerHeight, cellSize, byWidth};
}

function addBaseElements() {
    const bodyStyle = document.body.style;

    bodyStyle.overflow = 'hidden';

    bodyStyle.height = '100vh';
    bodyStyle.minHeight = '100vh';

    bodyStyle.margin = '0px';
    bodyStyle.padding = '0px';
    bodyStyle.display = 'flex';
    bodyStyle.justifyContent = 'center';
    bodyStyle.alignItems = 'center';

    const container = document.createElement('div');
    container.id = MAIN_CONTAINER_ID;
    document.body.appendChild(container);
}

function setMainContainerProperties({containerWidth, containerHeight}) {
    const container = document.getElementById(MAIN_CONTAINER_ID);
    container.style.width = containerWidth.toFixed(0) + 'px';
    container.style.height = containerHeight.toFixed(0) + 'px';

    //Autoalign children in the center of the container
    container.style.display = 'flex';
    container.style.justifyContent = 'center';
    container.style.alignItems = 'center';
}

function addField(fieldWidth, fieldHeight) {
    if (document.getElementById(FIELD_ID) !== null) return;

    const table = createTable(fieldWidth, fieldHeight, FIELD_BORDER);
    table.style.width = 100 * FIELD_SIZE_WIDTH + '%';
    table.style.height = '100%';
    table.id = FIELD_ID;

    document.getElementById(MAIN_CONTAINER_ID).appendChild(table);
}

function createTable(width, height, border) {
    const table = document.createElement('table');

    table.style.borderCollapse = 'collapse';

    const tbody = document.createElement('tbody');

    for(let h = 1; h <= height; h++) {
        const tr = tbody.insertRow();

        for (let w = 1; w <= width; w++) {
            const td = tr.insertCell();
            td.style.border = border;
        }
    }

    table.appendChild(tbody);

    return table;
}

function addInfo() {
    if (document.getElementById(INFO_ID) !== null) return;

    const container = document.getElementById(MAIN_CONTAINER_ID);
    container.style.fontFamily = FONT_NAME;

    //Main Info container
    const infoContainer = document.createElement('div');
    infoContainer.id = INFO_ID;

    infoContainer.style.width = 100 * (1 - FIELD_SIZE_WIDTH) + '%';
    infoContainer.style.height = '100%';
    
    infoContainer.style.display = 'flex';
    infoContainer.style.flexDirection = 'column';
    infoContainer.style.justifyContent = 'space-around';

    container.appendChild(infoContainer);

    //Score
    infoContainer.appendChild(createTextContainer('SCORE', 0, SCORE_ID));

    //Next tetromino
    infoContainer.appendChild(createNextTetrominoContainer('NEXT', NEXT_TETROMINO_FIELD_SIZE, NEXT_TETROMINO_FIELD_ID));

    //Level
    infoContainer.appendChild(createTextContainer('LEVEL', 1, LEVEL_ID));
}

function createNextTetrominoContainer(labelName, fieldSize, tableID) {
    const container = document.createElement('div');

    container.style.display = 'flex';
    container.style.flexDirection = 'column';
    container.style.alignItems = 'center';
    container.style.justifyContent = 'center';

    const label = document.createElement('div');
    label.innerHTML = labelName.toString();

    container.appendChild(label);

    const table = createTable(fieldSize, fieldSize, NEXT_TETROMINO_FIELD_BORDER);
    table.id = tableID;

    container.appendChild(table);

    return container;
}

function createTextContainer(labelName, defaultValue, valueID) {
    const container = document.createElement('div');

    container.style.display = 'flex';
    container.style.flexDirection = 'column';
    container.style.justifyContent = 'center';

    const label = document.createElement('div');
    label.innerHTML = labelName.toString();
    label.style.textAlign = 'center';

    container.appendChild(label);
    
    const value = document.createElement('div');
    value.id = valueID;
    value.innerHTML = defaultValue.toString();
    value.style.textAlign = 'center';

    container.appendChild(value);

    return container;
}

function updateTable(matrix, tableID) {
    const table = document.getElementById(tableID);
    
    for(let h = 0; h < matrix.length; h++) {
        const tr = table.rows[h];

        for(let w = 0; w < matrix[h].length; w++) {
            updateCell(tr.cells[w], matrix[h][w]);
        }
    }
}

function updateCell(cell, data) {
    switch (data) {
    case 0:
        cell.style.backgroundColor = '#FFFFFF';
        break;    
    default:
        cell.style.backgroundColor = '#000000';
        break;
    }
}

function updateInfo(score = 0, level = 0, tetrominoMatrix = [[0]]) {
    document.getElementById(SCORE_ID).innerHTML = score.toString();
    document.getElementById(LEVEL_ID).innerHTML = level.toString();

    const fullMatrix = fillMatrix(tetrominoMatrix, NEXT_TETROMINO_FIELD_SIZE);

    updateTable(fullMatrix, NEXT_TETROMINO_FIELD_ID);
}

function fillMatrix(matrix, toSize) {
    const newMatrix = matrix;

    for (let column = newMatrix[0].length; column < toSize; column++) {
        newMatrix.forEach((row) => {
            if (column % 2 === 0) {
                row.unshift(0);
            } else {
                row.push(0);
            }
        });
    }

    for (let row = newMatrix.length; row < toSize; row++) {
        if (row % 2 === 0) {
            newMatrix.unshift(new Array(toSize).fill(0));
        } else {
            newMatrix.push(new Array(toSize).fill(0));
        }
    }

    return newMatrix;
}

function resizeContainer({containerWidth, containerHeight}) {
    const container = document.getElementById(MAIN_CONTAINER_ID);
    container.style.width = containerWidth.toFixed(0) + 'px';
    container.style.height = containerHeight.toFixed(0) + 'px';
}

function resizeFont({byWidth = false}) {
    const main = document.getElementById(MAIN_CONTAINER_ID);

    if(byWidth) {
        main.style.fontSize = FONT_SIZE / (main.clientWidth / main.clientHeight) + 'vw';
        return;
    }

    main.style.fontSize = FONT_SIZE + 'vh';
}

function resizeNextTetrominoTable({cellSize}) {
    const size = cellSize * NEXT_TETROMINO_FIELD_SIZE + 'px';

    const nextTetrominoTable = document.getElementById(NEXT_TETROMINO_FIELD_ID);
    nextTetrominoTable.style.width = size;
    nextTetrominoTable.style.height = size;
}

function onWindowResize() {
    if (isResizeRunning) return;

    isResizeRunning = true;

    setTimeout(() => {
        UI.instance.resizeGame();
        isResizeRunning = false;
    }, 100);
}

export {UI};
