import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './reducers/root-reducer';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import 'bootstrap/dist/css/bootstrap.css';

const defaultState = [
    { id: 1, content: 'mark this task as done', isDone: false },
    { id: 2, content: 'remove this task', isDone: false },
    { id: 3, content: 'add more tasks to the list', isDone: false },
];

const store = createStore(rootReducer, defaultState);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

 serviceWorker.unregister();
