import { ADD_TODO, CHANGE_TODO, REMOVE_TODO, TOGGLE_TODO } from '../constants/action-types';

export const addToDo = content => ({
    type: ADD_TODO,
    content
});

export const changeToDo = ({ id, content }) => ({
    type: CHANGE_TODO,
    content,
    id
});

export const removeToDo = id => ({
    type: REMOVE_TODO,
    id
});

export const toggleToDo = id => ({
    type: TOGGLE_TODO,
    id
});


