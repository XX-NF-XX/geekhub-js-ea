import { ADD_TODO, CHANGE_TODO, REMOVE_TODO, TOGGLE_TODO } from '../constants/action-types';

function getNextID (todos) {
    let id = 1;
    if (todos.length) {
        id += Math.max(...todos.map(({ id }) => id));
    }
    return id;
}

const rootReducer = (state = [], action) => {
    switch (action.type) {
        case ADD_TODO:
            return [
                ...state,
                {
                    id: getNextID(state),
                    content: action.content,
                    isDone: false
                }
            ];
        case CHANGE_TODO:
            return state.map(todo => todo.id === action.id ? { ...todo, content: action.content } : todo );
        case REMOVE_TODO:
            return state.filter(todo => todo.id !== action.id)
        case TOGGLE_TODO:
            return state.map(todo => (todo.id === action.id ? { ...todo, isDone: !todo.isDone } : todo));
        default:
            return state;
    }
};

export default rootReducer;
