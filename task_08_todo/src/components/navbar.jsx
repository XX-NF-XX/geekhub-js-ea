import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

const mapStateToProps = (state, ownProps) => ({
    total: state.length,
    checked: state.filter(({ isDone }) => isDone).length
});

const connectedNavBar = ({ total, checked }) => {
    return (
        <nav className="navbar navbar-light bg-light justify-content-start">
            <span className="navbar-brand mb-0 h1">To Do List</span>
            <span className="badge badge-info m-1">{total}</span>
            <span className="badge badge-secondary m-1">{total - checked}</span>
            <span className="badge badge-success m-1">{checked}</span>
        </nav>
    );
};

connectedNavBar.propTypes = {
    total: PropTypes.number.isRequired,
    checked: PropTypes.number.isRequired
};

const NavBar = connect(mapStateToProps)(connectedNavBar);

export default NavBar;
