import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

import ToDo from './todo';
import AddToDo from './add-todo';

import { LIST_TODO } from '../constants/todo-list-types';

const mapStateToProps = (state, ownProps) => ({
    title: ownProps.title,
    hasAdd: ownProps.hasAdd,
    todos: state.filter(({ isDone }) => {
        if (ownProps.title === LIST_TODO) {
            return !isDone;
        }
        return isDone;
    }),
});

const connectedToDoList = ({ title, hasAdd, todos }) => {
    return (
        <div>
            <h5 className='m-2 text-center'>{title}</h5>
            <div>
                {hasAdd ? <AddToDo /> : ''}
                {todos.map(todo => <ToDo key={todo.id} todo={todo}/>)}
            </div>
        </div>
    );
};

connectedToDoList.propTypes = {
    title: PropTypes.string.isRequired,
    hasAdd: PropTypes.bool.isRequired,
    todos: PropTypes.array.isRequired
};

const ToDoList = connect(mapStateToProps)(connectedToDoList);

export default ToDoList;
