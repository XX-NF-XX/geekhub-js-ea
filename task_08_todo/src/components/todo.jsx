import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

import { changeToDo, removeToDo, toggleToDo } from '../actions/actions';
import { REMOVE_MARK, CHECK_MARK } from '../constants/marks';

const mapStateToProps = (state, { todo: { id, content, isDone } }) => ({ id, content, isDone });

const mapDispatchToProps = dispatch => {
    return {
        toggle: id => dispatch(toggleToDo(id)),
        change: options => dispatch(changeToDo(options)),
        remove: id => dispatch(removeToDo(id)),
    };
};

const connectedToDo = ({ id, content, isDone, toggle, change, remove }) => {
    return (
        <div className="input-group m-3">
            <div className="input-group-prepend">
                <button
                    className={isDone ? 'btn btn-outline-success' : 'btn btn-outline-secondary'}
                    type="button"
                    onClick={() => toggle(id)}
                >
                    {CHECK_MARK}
                </button>
            </div>
            <input
                className="form-control"
                value={content}
                onChange={ev => change({ content: ev.target.value, id })}
                type="text"
                placeholder="unnamed to do"
            />
            <div className="input-group-append">
                <button
                    onClick={() => remove(id)}
                    className="btn btn-outline-danger"
                    type="button"
                    id="button-addon2"
                >
                    {REMOVE_MARK}
                </button>
            </div>
        </div>
    );
};

connectedToDo.propTypes = {
    id: PropTypes.number.isRequired,
    content: PropTypes.string.isRequired,
    isDone: PropTypes.bool.isRequired,
    toggle: PropTypes.func.isRequired,
    change: PropTypes.func.isRequired,
    remove: PropTypes.func.isRequired
};

const ToDo = connect(mapStateToProps, mapDispatchToProps)(connectedToDo);

export default ToDo;
