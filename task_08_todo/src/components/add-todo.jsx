import { connect } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';

import { addToDo } from '../actions/actions';
import { ADD_MARK } from '../constants/marks';

const mapDispatchToProps = dispatch => {
    return {
        add: content => dispatch(addToDo(content))
    };
};

const connectedAddToDo = ({ add }) => {
    const inputRef = React.createRef();
    return (
        <div className="input-group m-3">
            <div className="input-group-prepend">
                <button
                    onClick={() => {
                        add(inputRef.current.value.toString());
                        inputRef.current.value = '';
                    }}
                    className="btn btn-outline-success"
                    type="button"
                >
                    {ADD_MARK}
                </button>
            </div>
            <input type="text" className="form-control" placeholder="description of your task" ref={inputRef} />
        </div>
    );
};

connectedAddToDo.propTypes = {
    add: PropTypes.func.isRequired
};

const AddToDo = connect(null, mapDispatchToProps)(connectedAddToDo);

export default AddToDo;
