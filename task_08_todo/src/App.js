import React, { Component } from 'react';
import ToDoList from './components/todo-list';
import NavBar from './components/navbar';
import { LIST_TODO, LIST_DONE } from './constants/todo-list-types';

class App extends Component {
    render() {
        return (
            <React.Fragment>
                <NavBar/>
                <main className='container'>
                    <ToDoList
                        title={LIST_TODO}
                        hasAdd={true}
                    />
                    <ToDoList
                        title={LIST_DONE}
                        hasAdd={false}
                    />
                </main>
            </React.Fragment>
        );
    }
}

export default App;
