var blocks = 5000;
var switchTimerLabel = "switch timer";
var ifTimerLabel = "if timer";

function startTest() {
    var switchTest = createSwitchFunction(blocks);

    console.time(switchTimerLabel);
    runFunction(switchTest, blocks);
    console.timeEnd(switchTimerLabel);

    var ifTest = createIfFunction(blocks);

    console.time(ifTimerLabel);
    runFunction(ifTest, blocks);
    console.timeEnd(ifTimerLabel);

    document.getElementById("logHint").style.visibility = "visible";
}

function createSwitchFunction(length) {
    var parameter = "integer";
    
    var code = "switch (" + parameter + ") { \n";

    for (var i = 0; i < length; i++) {
        code += "  case " + i + ":\n    break;\n";
    }

    code += "  default:\n    break;\n}";

    return new Function(parameter, code);
}

function createIfFunction(length) {
    var parameter = "integer";
    
    var code = "if (" + parameter + " === 0) {\n}";

    for (var i = 1; i < length; i++) {
        code += " else if (" + parameter + " === " + i + ") {\n}";
    }

    return new Function(parameter, code);
}

function runFunction (func, calls) {
    for (var i = 0; i < calls; i++) {
        func(i);
    }
}
