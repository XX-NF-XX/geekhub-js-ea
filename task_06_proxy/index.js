const http = require('http');
const fs = require('fs').promises;

const HOSTNAME = '127.0.0.1';
const PROXY_PORT = '8080';
const TEST_PORT = '8282';
const TEST_HEADER = './test.header.html';
const TEST_FOOTER = './test.footer.html';
const TEST_PAGE_JS = './test.js';

const PATH_BEGINS_ORIGIN = /^\/?http:\/\/[^/]*/i;
const PATH_BEGINS_HTTP = /^\/?(?!http:)\w{2,}:/;

let currentOrigin = '';

createProxy();
createTest();

// Proxy
function createProxy() {
    const proxyServer = http.createServer();

    proxyServer.on('request', (request, response) => {
        response.setHeader('Access-Control-Allow-Origin', '*');
        console.log(`Got request on Proxy with method: ${request.method}`);

        if (PATH_BEGINS_HTTP.test(request.url)) {
            response.writeHead(418, {'Content-Type': 'text/html', 'Access-Control-Allow-Origin': '*'});
            response.write('<html><body><h3>Status: 418 - I\'m a teapot</h3>');
            response.write('<p>The server refuses to brew coffee because it is a teapot.</p>');
            response.end('<p>Sorry. Only HTTP protocol is supported</p></body></html>');
            return;
        }

        if (PATH_BEGINS_ORIGIN.test(request.url)) {
            const requestedURL = new URL(request.url.slice(1));

            if (currentOrigin !== requestedURL) {
                currentOrigin = requestedURL.origin;
                response.writeHead(302,  {'Location': `http://${HOSTNAME}:${PROXY_PORT}${requestedURL.pathname}${requestedURL.search}`, 'Access-Control-Allow-Origin': '*'});
                response.end('Ok. Redirecting.');
                return;
            }
        }

        if (currentOrigin === '') {
            response.writeHead(200, {'Content-Type': 'text/html', 'Access-Control-Allow-Origin': '*'});
            response.end(`Origin is not set. To set origin add it to the adress bar e.g.: http://${HOSTNAME}:${PROXY_PORT}/http://learn.javascript.ru/intro or http://${HOSTNAME}:${PROXY_PORT}/http://${HOSTNAME}:${TEST_PORT}/`);
            return;
        }

        const originURL = new URL(currentOrigin);
        const requestHeaders = request.headers;
        requestHeaders.host = originURL.hostname;

        const options = {
            host: originURL.hostname,
            method: request.method,
            path: request.url,
            port: originURL.port,
            headers: requestHeaders,
        };

        const requestToServer = http.request(options, (serverMessage) => {

            const headers = modifyHeaders(serverMessage.headers);
            response.writeHead(serverMessage.statusCode, headers);
            console.log(headers);

            serverMessage.on('data', (chunk) => {
                response.write(chunk);
            }).on('end', () => {
                response.end();
            });
        });

        request.on('data', (chunk) => {
            console.log('>==>');
            requestToServer.write(chunk);
        });

        request.on('end', () => {
            requestToServer.end();
        });

        requestToServer.on('error', (err) => {
            console.error(`Problem with request: ${err.message}`);
            response.end('Problem with request to the original server');
            requestToServer.end();
        });
    });

    proxyServer.listen(PROXY_PORT, HOSTNAME, () => {
        console.log(`Proxy server is running at http://${HOSTNAME}:${PROXY_PORT}/`);
    });
}

// Test
function createTest() {
    const testServer = http.createServer();

    testServer.on('request', (request, response) => {
        console.log(`Got request on Test with method: ${request.method}`);

        // if ('.' + request.url === TEST_PAGE_JS) {
        //     responseWithFile(TEST_PAGE_JS, response, 'text/javascript');
        //     return;
        // }

        const data = [];

        request.on('data', (chunk) => {
            data.push(chunk);
        }).on('end', () => {
            responseWithIndexPage(request, response, data);
        }).on('error', (err) => {
            console.error(`Cannot receive request: ${err.message}`);
            response.writeHead(400, {'Content-Type': 'text/html'});
            response.end('Cannot receive request!');
        });
    });
    
    testServer.listen(TEST_PORT, HOSTNAME, () => {
        console.log(`Test server running at http://${HOSTNAME}:${TEST_PORT}/`);
    });
}

function modifyHeaders(headers) {
    if (headers.hasOwnProperty('X-Frame-Options')) {
        delete headers['X-Frame-Options'];
    }

    return headers;
}

async function responseWithFile(file, response, contentType) {
    try {
        const result = await fs.readFile(file, 'utf8');
        response.writeHead(200, {'Content-Type': contentType});
        response.end(result);
    } catch (error) {
        response.writeHead(500, {'Content-Type': 'text/html'});
        response.end(`Cannot read file ${file}: ${error.code}`);
        console.log(error);
    }
}

async function responseWithIndexPage(request, response, data) {
    
    await writeIndexHeader(response);
    await writeIndexInfo(request, response, data);
    await writeIndexFooter(response);
    response.end();

    async function writeIndexHeader(response) {
        try {
            const header = await fs.readFile(TEST_HEADER, 'utf8');
            response.writeHead(200, {'Content-Type': 'text/html'});
            response.write(header);
        } catch (error) {
            response.writeHead(500, {'Content-Type': 'text/html'});
            response.end(`Cannot read file ${TEST_HEADER}: ${error.code}`);
            console.log(error);
        }
    }

    async function writeIndexInfo(request, response, data) {
        response.write(getInfoHeader(request));
        response.write(Buffer.concat(data).toString());
        response.write(getInfoFooter());
    }

    async function writeIndexFooter(response) {
        try {
            const footer = await fs.readFile(TEST_FOOTER, 'utf8');
            response.write(footer);
        } catch (error) {
            response.writeHead(500, {'Content-Type': 'text/html'});
            response.end(`Cannot read file ${TEST_FOOTER}: ${error.code}`);
            console.log(error);
        }
    }
}

function getInfoHeader({url, method, httpVersion, rawHeaders}) {
    let result = `
    <div class="header">
        <h3>Info on your request:</h3>
        <p><b>Request path:</b> ${url}</p>
        <p><b>Request method:</b> ${method}</p>
        <p><b>Version:</b> ${httpVersion}</p>
        <p><b>Request headers:</b></p>
        <ul>` + '\n';

    result += rawHeaders.reduce((acc, value, idx) => {
        return acc += (idx % 2) === 0 ? '\t\t\t' + `<el><p>${value.toString()}: ` : `${value.toString()}</p></el>` + '\n';  
    }, '');

    result += '\t\t\t</ul>\n\t\t<p><b>Request body:</b></p>\n\t</div>\n\t\t<div>';

    return result;
}

function getInfoFooter() {
    return '\n\t\t</div>\n';
}
