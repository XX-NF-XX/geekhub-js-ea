var bracketSymbols = [{ 
    openSymbol: "{", 
    closeSymbol: "}"
 }, {
     openSymbol: "[",
     closeSymbol: "]"
 }, {
     openSymbol: "(",
     closeSymbol: ")"
 }];

var quoteSymbols = [ "\"", "'", "`"];

function startChecking() {
    var textToCheck = document.getElementById("textToCheck").value;
    var ignoreEscaped = document.getElementById("ignoreEscapedText").checked;
    var ignoreQuoted = document.getElementById("ignoreQuotedText").checked;
    var ignoreQuotesInQuoted = document.getElementById("ignoreQuotesInQuotedText").checked;

    var symbolsAndPositions = getSymbolsAndTheirPositions(textToCheck);

    symbolsAndPositions = removeInQuotedText(textToCheck, symbolsAndPositions, ignoreEscaped, ignoreQuoted, ignoreQuotesInQuoted);

    var error = checkForClosingSymbols(symbolsAndPositions, textToCheck);

    document.getElementById("result").innerHTML = error;
}

function getSymbolsAndTheirPositions(text) {
    var foundSymbolsAndPositions = [];

    for (var i = 0; i < text.length; i++) {
        var isSymbolFound = isRequiredSymbol(text[i]);
        
        if (isSymbolFound) {
            foundSymbolsAndPositions.push( { symbol: text[i], position: i } );
        }
    }

    return foundSymbolsAndPositions;
}

function isRequiredSymbol(symbol) {
 
    for (var i = 0; i < bracketSymbols.length; i++) {
        if (symbol === bracketSymbols[i].openSymbol || symbol === bracketSymbols[i].closeSymbol) {
            return true;
        }
    }

    if (isQuoteSymbol(symbol)) {
        return true;
    }

    return false;
}

function isQuoteSymbol(symbol) {
    for (var i = 0; i < quoteSymbols.length; i++) {
        if (symbol === quoteSymbols[i]) {
            return true;
        }
    }
    return false;
}

function checkForClosingSymbols(symbolsAndPositions, textToCheck) {
    var error = { desription: "" };
    
    for (var i = 0; i < symbolsAndPositions.length; i++) {
        i = findPair(symbolsAndPositions, i, error, textToCheck);
    }

    if (error.desription === "") {
        return "OK";
    } else {
        return error.desription;
    }
}

function findPair(symbolsAndPositions, index, error, textToCheck) {
    if (index >= symbolsAndPositions.length) {
        return symbolsAndPositions.length;
    }

    var openSymbol = symbolsAndPositions[index].symbol;

    if (!isOpenSymbol(openSymbol)) {
        error.desription += "Unexpected symbol " + openSymbol + " at " + 
            getReadableSymbolPosition(symbolsAndPositions[index].position, textToCheck) + "\n";
        return index;
    }

    var closeSymbol = getClosingSymbol(openSymbol);

    if (closeSymbol === null) {
        error.desription += "Cannot find closing symbol for " + openSymbol + " at " +
            getReadableSymbolPosition(symbolsAndPositions[index].position, textToCheck) + "\n";
        return symbolsAndPositions.length;
    }

    for (var i = index + 1; i < symbolsAndPositions.length; i++) {
        if (symbolsAndPositions[i].symbol === closeSymbol) {
            return i;
        } else {
            i = findPair(symbolsAndPositions, i, error, textToCheck);

            if (i >= symbolsAndPositions.length) {
                error.desription += "Cannot find " + closeSymbol + " for " + openSymbol + " at " +
                    getReadableSymbolPosition(symbolsAndPositions[index].position, textToCheck) + "\n";
                return symbolsAndPositions.length;
            }
        }
    }

    error.desription += "Cannot find " + closeSymbol + " for " + openSymbol + " at " +
        getReadableSymbolPosition(symbolsAndPositions[index].position, textToCheck) + "\n";
    return symbolsAndPositions.length;
}

function getClosingSymbol(openSymbol) {
    for (var i = 0; i < bracketSymbols.length; i++) {
        if (bracketSymbols[i].openSymbol === openSymbol) {
            return bracketSymbols[i].closeSymbol;
        }
    }

    if (isQuoteSymbol(openSymbol)) {
        return openSymbol;
    }

    console.error("Closing symbol not found for '" + openSymbol + "'");
    return null;
}

function isOpenSymbol(symbol) {
    for (var i = 0; i < bracketSymbols.length; i++) {
        if (bracketSymbols[i].openSymbol === symbol) {
            return true;
        }
    }

    if (isQuoteSymbol(symbol)) {
        return true;
    }

    return false;
}

//when removeEverything == true it'll remove everything excep escaped quotes unless removeEscapedQuotes == true;
function removeInQuotedText (textToCheck, symbolsAndPositions, removeEscapedQuotes = false, removeEverything = false, removeInnerQuotes = false) {
    var modifiedArray = symbolsAndPositions.slice(); //Make an array copy
    var openingQuote = null;

    for (var i = 0; i < modifiedArray.length; i++) {
        var currentSymbol = modifiedArray[i].symbol;
        var currentSymbolPosition = modifiedArray[i].position;

        if (openingQuote === null) {
            if (isQuoteSymbol(currentSymbol)) {
                openingQuote = currentSymbol;
                continue;
            }
        } else {
            if (currentSymbol === openingQuote) {
                if (removeEscapedQuotes && textToCheck[currentSymbolPosition - 1] === "\\") {
                    modifiedArray.splice(i, 1);
                    i--;
                } else {
                    openingQuote = null;
                }
                continue;
            } 
        }

        if (removeEverything) {
            modifiedArray.splice(i, 1);
            i--;
        } else if (removeInnerQuotes && isQuoteSymbol(currentSymbol)) {
            modifiedArray.splice(i, 1);
            i--;
        }
    }
    return modifiedArray;
}

function getReadableSymbolPosition(endIndex, text) {
    var textToSearch = text.substring(0, endIndex);
    var linePosition = 1;
    var newlineIndex = -1;
    var characterPosition = 1;

    for (var i = 0; i < textToSearch.length; i++) {
        var index = textToSearch.indexOf("\n", i);

        if (index != -1) {
            newlineIndex = index;
            linePosition++;
            i = index;
        } else {
            characterPosition = (endIndex + 1) - (newlineIndex + 1);
            break;
        }
    }

    return "line: " + linePosition + " character: " + characterPosition;
}

function useSample() {
    var sample = "var a = \"A huge 70\\\" flat TV\";\nvar b = 'It\\\'s 10 o\\\'clock';\n" + 
                 "function (param) {\n  var arr = [[1, 2], [3, 3]];\n  console.log(\"param is \" + param);\n}\n" + 
                 "var c = `Quotes: \\` \" ' and brackets: ( ) { } [ ]`;";
    document.getElementById("textToCheck").value = sample;
}

function toggleDisableOfIgnoreQuotesCheckbox() {
    var isChecked = document.getElementById("ignoreQuotedText").checked;
    document.getElementById("ignoreQuotesInQuotedText").disabled = isChecked;
}