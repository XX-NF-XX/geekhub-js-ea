const ADULT_AGE = 18;
const DNI_LENGHT = 8;

class Person {
    constructor(name = '', age = 0, sex = 'M', weight = 0, height = 0) {
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.height = height;

        this.createDNI();
    }

    calculateIMC() {
        if (this._height == 0) throw new Error('Cannot devide by zero');
        const result =  this._weight / (this._height * this._height);

        if (result < 20) return -1;
        if (result <= 25) return 0;
        return 1;
    }

    isAdult() {
        return this._age >= ADULT_AGE ? 1 : 0;
    }

    checkSex(sex) {
        this._sex = sex !== 'F' ? 'M' : sex;
    }

    toString() {
        return `Name: ${this._name} Age: ${this._age} y.o Sex: ${this._sex} DNI: ${this._DNI} Weight: ${this._weight} kg Height: ${this._height} m`;
    }

    createDNI() {
        const minDNI = Math.pow(10, DNI_LENGHT - 1);
        const maxDNI = Math.pow(10, DNI_LENGHT) - 1;
        this._DNI = Math.trunc((maxDNI - minDNI) * Math.random() + minDNI);
    }

    set name(name) {
        this._name = name.toString();
    }

    set age(age) {
        if (Number.isNaN(age) || age < 0) throw new Error('Age must be a number that equal to or bigger than zero');
        this._age = age; 
    }

    set sex(sex) {
        this.checkSex(sex);
    }

    set weight(weight) {
        if (Number.isNaN(Number.parseFloat(weight)) || weight < 0) {
            throw new Error('Weight must be a number that equal to or bigger than zero');
        }
        this._weight = weight;
    }

    set height(height) {
        if (Number.isNaN(Number.parseFloat(height)) || height < 0) {
            throw new Error('Height must be a number that equal to or bigger than zero');
        }
        this._height = height;
    }
}

class Test {
    static run() {
        const name = 'John';
        const age = 20;
        const sex = 'M';
        const weight = 70;
        const height = 1.76;

        const persons = [];

        persons.push(new Person(name, age, sex, weight, height));
        persons.push(new Person(name, age, sex));
        
        const peppa = new Person(); 
        persons.push(peppa);

        peppa.name = 'Peppa';
        peppa.age = 5;
        peppa.sex = 'F';
        peppa.weight = 18;
        peppa.height = 0.8;

        persons.forEach(logIMC);

        persons.forEach(logAdultness);

        persons.forEach((person) => console.log(person.toString()));

        function logIMC(person) {
            let result;
            try {
                result = person.calculateIMC();
            } catch (err) {
                console.error(`Cannot calculate IMC for ${person._name}: ${err.message}`);
                return;
            }

            let phrase = '';

            if (result === -1) {
                phrase = 'underweight';
            } else if (result === 1) {
                phrase = 'overweight';
            } else {
                phrase = 'normal weight';
            }

            console.log(`${person._name}\'s weight test: ${phrase}.`);
        }

        function logAdultness(person) {
            let phrase = 'is';
            if (!person.isAdult()) phrase = 'is not';

            console.log(`${person._name} ${phrase} an adult`);
        }
    }
}

Test.run();
