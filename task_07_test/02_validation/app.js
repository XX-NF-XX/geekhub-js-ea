const ORANGE_BORDER = '0.2em solid #FFCC00';
const GREEN_BORDER = '0.2em solid #00EE00';

const EMAIL_REGEX = /^\S+@\S+\.\S+$/;
const SPEC_CHAR_REGEX = /\W|_/;

const email = document.getElementById('email');
const password = document.getElementById('pwd');
const repeat = document.getElementById('repeatPwd');
const form = document.getElementById('mainForm');
const hint = document.getElementById('hint');

hint.style.color = '#FF4444';

form.onsubmit = validate;

password.onkeyup = highlightStrongPassword;

function validate(event) {
    try {
        validateEmail(email.value);
        validatePassword(password.value, repeat.value);
    } catch (err) {
        hint.innerHTML = err.message;
        event.preventDefault();
    }
}

function validateEmail(email) {
    if (!EMAIL_REGEX.test(email.trim())) throw new Error('Invalid email address');
}

function highlightStrongPassword() {
    this.style.border = SPEC_CHAR_REGEX.test(this.value) ? GREEN_BORDER : ORANGE_BORDER;
}

function validatePassword(password, repeat) {
    if (password !== repeat) throw new Error('Passwords do not match each other');

    if (password.length <= 6) throw new Error('Password must contain at least 7 characters');
}
