/*eslint no-console: "warn", no-unused-vars: "warn" */

(function () {

    var globalObject;

    try {
        globalObject = global; //node
    } catch (err) {
        try {
            globalObject = window; //browser
        } catch (err) {
            throw new Error("Cannot find the global object");
        }
    }

    try {
        if (typeof globalObject.Message !== "undefined") {
            throw new Error("Variable 'Message' has already been defined!");
        } else if (typeof globalObject.Chat !== "undefined") {
            throw new Error("Variable 'Chat' has already been defined!");
        } else if (typeof globalObject.User !== "undefined") {
            throw new Error("Variable 'User' has already been defined!");
        }
    } catch (err) {
        console.error("Cannot create constructor: " + err.message);
        throw err;
    }

    
    /**
     * Represents a chat.
     * @constructor
     * @param {string} chatName - The name of the chat.
     */
    var Chat = function (chatName) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof chatName !== "string", "Argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of Chat: " + err.message);
            throw err;
        }

        var name = chatName;
        var users = [];
        var messages = [];
  
        Chat.chats.push(this);

        /** Gets the chat name 
         * @return {string} chat name
         */
        this.getName = function() {
            return name;
        };

        /** Connects user(s) to the chat
         * @param {...User} user - user(s) to be connected
         */
        this.join = function (user) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                for (var i = 0; i < arguments.length; i++) {
                    throwError(arguments[i] instanceof User === false, "Argument " + i + " is not an instance of User!", TypeError);
                }
            } catch (err) {
                console.error("Cannot connect to Chat: " + err.message);
                throw err;
            }

            for (var i = 0; i < arguments.length; i++) {
                var currentUser = arguments[i];

                if (!this.isJoined(currentUser)) {
                    users.push(currentUser);
                } else {
                    console.warn("User is already connected to the chat.");
                }
            }
        };

        /** Disconnects user(s) from the chat
         * @param {...User} user - user(s) to be disconnected
         */
        this.leave = function (user) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                for (var i = 0; i < arguments.length; i++) {
                    throwError(arguments[i] instanceof User === false, "Argument " + i + " is not an instance of User!", TypeError);
                }
            } catch (err) {
                console.error("Cannot disconnect from Chat: " + err.message);
                throw err;
            }

            for (var i = 0; i < arguments.length; i++) {
                var currentUser = arguments[i];

                var userIdx = users.findIndex(function (element) {
                    return element == this;
                }, currentUser);

                if (userIdx >= 0) {
                    users.splice(userIdx, 1);
                } else {
                    console.warn("Cannot disconnect user. User isn't connected.");
                }
            }
        };

        /** Checks whether user joined to the chat or not
         * @param {User} user - user to check
         * @return {boolean} true if user joined to the chat
         */
        this.isJoined = function (user) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
            } catch (err) {
                console.error("Cannot check whether user connected or not: " + err.message);
                throw err;
            }

            var userIdx = users.findIndex(function (element) {
                return element === this;
            }, user);

            if (userIdx >= 0) {
                return true;
            }
            return false;
        };

        /** Sends a message to the chat
         * @param {User} user - user who sends the message
         * @param {string} textMessage - message to be sent
         * @return {Message} sent message
         */
        this.sendMessage = function (user, textMessage) {
            try {
                throwError(arguments.length < 2, "Not enough arguments!", ReferenceError);
                throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
                throwError(typeof textMessage !== "string", "Second argument is not a string!", TypeError);
            } catch (err) {
                console.error("Cannot send message: " + err.message);
                throw err;
            }

            if (this.isJoined(user)) {
                var message = new Message (user, textMessage);
                messages.push(message);

                return message;
            } else {
                throw new Error("Cannot send message! You have to join the chat to send a message.");
            }
        };

        /** Logs chat history to console
         * @param {number} [index] - index to start from
         * @param {number} amount - amount of messages to log
         */
        this.logChatHistory = function (index, amount) {
            var defaultIndex = 0;
            var defaultAmount = 10;

            if (arguments.length >= 2 ) {
                index = arguments[0];
                amount = arguments[1];

            } else if (arguments.length == 1) {
                amount = arguments[0];
                index = defaultIndex;

            } else {
                index = defaultIndex;
                amount = defaultAmount;
            }

            try {
                throwError(typeof index !== "number", "Index must be a number!", TypeError);
                throwError(typeof amount !== "number", "Amount must be a number!", TypeError);
            } catch (err) {
                console.error("Cannot log chat history: " + err.message);
                throw err;
            }            

            for (var i = index; i < messages.length; i++) {
                if (i - index < amount) {
                    this.logMessage(messages[i]);
                } else {
                    break;
                }
            }
        };

        /** Logs to console a message
         * @param {Message} message - message to log
         */
        this.logMessage = function (message) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(message instanceof Message === false, "Argument is not an instance of Message!", TypeError);
            } catch (err) {
                console.error("Cannot log message: " + err.message);
                throw err;
            }

            var ownerStatus = "offline";

            if (this.isJoined(message.getOwner())) {
                ownerStatus = "online";
            }

            var messageDate = message.getDateOfCreation();
            var timeOptions = { hour: "numeric", minute: "numeric", second: "numeric"};
            var messageTime = messageDate.toLocaleTimeString("uk-UA", timeOptions) + "." + 
                      messageDate.getMilliseconds();

            console.log("[" + message.getOwner().getName() + "] {" + ownerStatus + "} [" + 
                messageTime + "] : " + message.getMessage());
        };
    
        /** Gets new messages
         * @param {User} user - user to get messages for
         * @param {number} [amount] - amount of messages to log
         * @return {Array} array of new messages 
         */
        this.getNewMessages = function (user, amount) {
            if (arguments.length === 1) {
                amount = Infinity;
            }

            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
                throwError(typeof amount !== "number", "Second argument is not a number!", TypeError);
            } catch (err) {
                console.error("Cannot get new messages: " + err.message);
                throw err;
            }

            var newMessages = messages.filter(function (message) {
                return (!message.isRead(user) && amount-- > 0);
            });
    
            newMessages.forEach(function (message) {
                message.markAsRead(user);
            });
            return newMessages;
        };
    };

    Chat.chats = [];

    /** Returns array of chats */
    Chat.getChats = function () {
        return Chat.chats;
    };

    Object.defineProperty(globalObject, "Chat", {
        configurable: false,
        get: function () { return Chat; },
        set: function () { throw new Error("Modifying of 'Chat' is forbidden!"); }
    });


    /**
     * Represents a message.
     * @constructor
     * @param {User} user - author of the message
     * @param {string} textMessage - the message
     */
    var Message = function (user, textMessage) {
        try {
            throwError(arguments.length < 2, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
            throwError(typeof textMessage !== "string", "Second argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of Message: " + err.message);
            throw err;
        }

        var message = textMessage;
        var owner = user;
        var createdOn = Date.now();
        var readByUsers = [owner];

        /** Marks message as read
         * @param {User} user - user that read the message
         */
        this.markAsRead = function (user) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
            } catch (err) {
                console.error("Cannot mark as read: " + err.message);
                throw err;
            }

            if (!this.isRead(user)) {
                readByUsers.push(user);
            }
        };

        /** Cheks whether message is marked as read
         * @param {User} user - user that read the message
         * @return {boolean} true if message is marked as read
         */
        this.isRead = function (user) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
            } catch (err) {
                console.error("Cannot check whether message is read or not: " + err.message);
                throw err;
            }

            var userIndex = readByUsers.findIndex(function (element) {
                return element == this;
            }, user);

            if (userIndex != -1) {
                return true;
            }
            return false;
        };

        /** Gets message author 
         *  @return {User} message author
         */
        this.getOwner = function () {
            return owner;
        };

        /** Gets date of creation 
         *  @return {Date} date of creation
         */
        this.getDateOfCreation = function () {
            return new Date(createdOn);
        };

        /** Gets text of the message 
         *  @return {string} text message
         */
        this.getMessage = function () {
            return message;
        };
    };

    Object.defineProperty(globalObject, "Message", {
        configurable: false,
        get: function () { return Message; },
        set: function () { throw new Error("Modifying of 'Message' is forbidden!"); }
    });


    /**
     * Represents a user.
     * @constructor
     * @param {string} userName - name of the user
     */
    var User = function (userName) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof userName !== "string", "userName argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of User: " + err.message);
            throw err;
        }  

        var name = userName;
        var defaultChat;

        /** Gets the user name 
         * @return {string} user name
         */
        this.getName = function () {
            return name;
        };

        /** Sets user's default chat 
         * @param {Chat} chat - instance of Chat
         */
        this.setDefaultChat = function (chat) {
            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
            } catch (err) {
                console.error("Cannot set default chat: " + err.message);
                throw err;
            }
            
            defaultChat = chat;
        };

        /** Connects user to the chat
         * @param {Chat} [chat] - chat to connect to
         */
        this.joinChat = function (chat) {
            if (arguments.length < 1) {
                defaultChat.join(this);
            } else {
                try {
                    throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
                } catch (err) {
                    console.error("Cannot connect to chat: " + err.message);
                    throw err;
                }
    
                chat.join(this);
            }
        };

        /** Disconnects user from the chat
         * @param {Chat} [chat] - chat to disconnect from
         */    
        this.leaveChat = function (chat) {
            if (arguments.length < 1) {
                defaultChat.leave(this);
            } else {
                try {
                    throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
                } catch (err) {
                    console.error("Cannot disconnect from chat: " + err.message);
                    throw err;
                }

                chat.leave(this);
            }
        };

        /** Sends the message to the chat
         * @param {Chat} [chat] - the chat to send to
         * @param {string} message - the text message to send
         */
        this.sendMessage = function (chat, textMessage) {
            if (arguments.length > 1) {
                chat = arguments[0];
                textMessage = arguments[1];
            } else if (arguments.length == 1) {
                textMessage = arguments[0];
                chat = defaultChat;
            }

            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(typeof textMessage !== "string", "textMessage argument is not a string!", TypeError);
                throwError(chat instanceof Chat === false, "Chat argument is not an instance of Chat!", TypeError);
            } catch (err) {
                console.error("Cannot send message: " + err.message);
                throw err;
            }

            chat.sendMessage(this, textMessage);
        };
    
        /** 
         * Logs to console new message(s)
         * @param {Chat} chat - the chat to log messages from
         * @param {number} [amount=10] - amount of messages to log
         *//** 
         * Logs to console new message(s) from default chat
         * @param {number} amount - amount of messages to log
         */
        this.logNewMessages = function (chat, amount) {
            var defaultAmount = 10; 

            if (arguments.length == 1) {
                if (arguments[0] instanceof Chat) {
                    chat = arguments[0];
                    amount = defaultAmount;

                } else {
                    amount = arguments[0];
                    chat = defaultChat;
                }
            } else if (arguments.length == 2) {
                if (arguments[0] instanceof Chat) {
                    chat = arguments[0];
                    amount = arguments[1];

                } else {
                    chat = arguments[1];
                    amount = arguments[0];
                }
            }

            try {
                throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
                throwError(typeof amount !== "number", "Amount argument is not a number!", TypeError);
                throwError(chat instanceof Chat === false, "Chat argument is not an instance of Chat!", TypeError);
            } catch (err) {
                console.error("Cannot log new messages: " + err.message);
                throw err;
            }

            var messages = chat.getNewMessages(this, amount);

            for (var i = 0; i < messages.length; i++) {
                chat.logMessage(messages[i]);
            }
        };
    };

    Object.defineProperty(globalObject, "User", {
        configurable: false,
        get: function () { return User; },
        set: function () { throw new Error("Modifying of 'User' is forbidden!"); }
    });

    /** Throws new error if condtition is true
     * @param {boolean} condition - condition
     * @param {string} message - error message
     * @param {Error | TypeError | ReferenceError} [errorType] - type of error object
     */
    function throwError (condition, message, errorType) {
        if (typeof globalObject.Message === "undefined") {
            errorType = Error;
        }

        if (condition) {
            throw new errorType(message);
        }
    }
})();
