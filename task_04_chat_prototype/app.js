/*eslint no-console: "warn", no-unused-vars: "warn" */

(function () {

    var globalObject;

    try {
        globalObject = global; //node
    } catch (err) {
        try {
            globalObject = window; //browser
        } catch (err) {
            throw new Error("Cannot find the global object");
        }
    }

    try {
        if (typeof globalObject.Message !== "undefined") {
            throw new Error("Variable 'Message' has already been defined!");
        } else if (typeof globalObject.Chat !== "undefined") {
            throw new Error("Variable 'Chat' has already been defined!");
        } else if (typeof globalObject.User !== "undefined") {
            throw new Error("Variable 'User' has already been defined!");
        }
    } catch (err) {
        console.error("Cannot create constructor: " + err.message);
        throw err;
    }

    
    /**
     * Represents a chat.
     * @constructor
     * @param {string} chatName - The name of the chat.
     */
    var Chat = function (chatName) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof chatName !== "string", "Argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of Chat: " + err.message);
            throw err;
        }

        this.name = chatName;
        this.users = [];
        this.messages = [];
  
        Chat.chats.push(this);
    };

    Chat.chats = [];

    /** Returns array of chats */
    Chat.getChats = function () {
        return Chat.chats;
    };

    /** Gets the chat name 
     * @return {string} chat name
     */
    Chat.prototype.getName = function() {
        return this.name;
    };

    /** Connects user(s) to the chat
     * @param {...User} user - user(s) to be connected
     */
    Chat.prototype.join = function (user) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            for (var i = 0; i < arguments.length; i++) {
                throwError(arguments[i] instanceof User === false, "Argument " + i + " is not an instance of User!", TypeError);
            }
        } catch (err) {
            console.error("Cannot connect to Chat: " + err.message);
            throw err;
        }

        for (var i = 0; i < arguments.length; i++) {
            var currentUser = arguments[i];

            if (!this.isJoined(currentUser)) {
                this.users.push(currentUser);
            } else {
                console.warn("User is already connected to the chat.");
            }
        }
    };

    /** Disconnects user(s) from the chat
     * @param {...User} user - user(s) to be disconnected
     */
    Chat.prototype.leave = function (user) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            for (var i = 0; i < arguments.length; i++) {
                throwError(arguments[i] instanceof User === false, "Argument " + i + " is not an instance of User!", TypeError);
            }
        } catch (err) {
            console.error("Cannot disconnect from Chat: " + err.message);
            throw err;
        }

        for (var i = 0; i < arguments.length; i++) {
            var currentUser = arguments[i];

            var userIdx = this.users.findIndex(function (element) {
                return element == this;
            }, currentUser);

            if (userIdx >= 0) {
                this.users.splice(userIdx, 1);
            } else {
                console.warn("Cannot disconnect user. User isn't connected.");
            }
        }
    };

    /** Checks whether user joined to the chat or not
     * @param {User} user - user to check
     * @return {boolean} true if user joined to the chat
     */
    Chat.prototype.isJoined = function (user) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
        } catch (err) {
            console.error("Cannot check whether user connected or not: " + err.message);
            throw err;
        }

        var userIdx = this.users.findIndex(function (element) {
            return element === this;
        }, user);

        if (userIdx >= 0) {
            return true;
        }
        return false;
    };

    /** Sends a message to the chat
     * @param {User} user - user who sends the message
     * @param {string} textMessage - message to be sent
     * @return {Message} sent message
     */
    Chat.prototype.sendMessage = function (user, textMessage) {
        try {
            throwError(arguments.length < 2, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
            throwError(typeof textMessage !== "string", "Second argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot send message: " + err.message);
            throw err;
        }

        if (this.isJoined(user)) {
            var message = new Message (user, textMessage);
            this.messages.push(message);

            return message;
        } else {
            throw new Error("Cannot send message! You have to join the chat to send a message.");
        }
    };

    /** Logs chat history to console
     * @param {number} [index] - index to start from
     * @param {number} amount - amount of messages to log
     */
    Chat.prototype.logChatHistory = function (index, amount) {
        var defaultIndex = 0;
        var defaultAmount = 10;

        if (arguments.length >= 2 ) {
            index = arguments[0];
            amount = arguments[1];

        } else if (arguments.length == 1) {
            amount = arguments[0];
            index = defaultIndex;

        } else {
            index = defaultIndex;
            amount = defaultAmount;
        }

        try {
            throwError(typeof index !== "number", "Index must be a number!", TypeError);
            throwError(typeof amount !== "number", "Amount must be a number!", TypeError);
        } catch (err) {
            console.error("Cannot log chat history: " + err.message);
            throw err;
        }            

        for (var i = index; i < this.messages.length; i++) {
            if (i - index < amount) {
                this.logMessage(this.messages[i]);
            } else {
                break;
            }
        }
    };

    /** Logs to console a message
     * @param {Message} message - message to log
     */
    Chat.prototype.logMessage = function (message) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(message instanceof Message === false, "Argument is not an instance of Message!", TypeError);
        } catch (err) {
            console.error("Cannot log message: " + err.message);
            throw err;
        }

        var ownerStatus = "offline";

        if (this.isJoined(message.getOwner())) {
            ownerStatus = "online";
        }

        var messageDate = message.getDateOfCreation();
        var timeOptions = { hour: "numeric", minute: "numeric", second: "numeric"};
        var messageTime = messageDate.toLocaleTimeString("uk-UA", timeOptions) + "." + 
                    messageDate.getMilliseconds();

        console.log("[" + message.getOwner().getName() + "] {" + ownerStatus + "} [" + 
            messageTime + "] : " + message.getMessage());
    };

    /** Gets new messages
     * @param {User} user - user to get messages for
     * @param {number} [amount] - amount of messages to log
     * @return {Array} array of new messages 
     */
    Chat.prototype.getNewMessages = function (user, amount) {
        if (arguments.length === 1) {
            amount = Infinity;
        }

        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
            throwError(typeof amount !== "number", "Second argument is not a number!", TypeError);
        } catch (err) {
            console.error("Cannot get new messages: " + err.message);
            throw err;
        }

        var newMessages = this.messages.filter(function (message) {
            return (!message.isRead(user) && amount-- > 0);
        });

        newMessages.forEach(function (message) {
            message.markAsRead(user);
        });
        return newMessages;
    };

    Object.defineProperty(globalObject, "Chat", {
        configurable: false,
        get: function () { return Chat; },
        set: function () { throw new Error("Modifying of 'Chat' is forbidden!"); }
    });


    /**
     * Represents a message.
     * @constructor
     * @param {User} user - author of the message
     * @param {string} textMessage - the message
     */
    var Message = function (user, textMessage) {
        try {
            throwError(arguments.length < 2, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "First argument is not an instance of User!", TypeError);
            throwError(typeof textMessage !== "string", "Second argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of Message: " + err.message);
            throw err;
        }

        this.message = textMessage;
        this.owner = user;
        this.createdOn = Date.now();
        this.readByUsers = [this.owner];
    };

    /** Marks message as read
     * @param {User} user - user that read the message
     */
    Message.prototype.markAsRead = function (user) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
        } catch (err) {
            console.error("Cannot mark as read: " + err.message);
            throw err;
        }

        if (!this.isRead(user)) {
            this.readByUsers.push(user);
        }
    };

    /** Cheks whether message is marked as read
     * @param {User} user - user that read the message
     * @return {boolean} true if message is marked as read
     */
    Message.prototype.isRead = function (user) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(user instanceof User === false, "Argument is not an instance of User!", TypeError);
        } catch (err) {
            console.error("Cannot check whether message is read or not: " + err.message);
            throw err;
        }

        var userIndex = this.readByUsers.findIndex(function (element) {
            return element == this;
        }, user);

        if (userIndex != -1) {
            return true;
        }
        return false;
    };

    /** Gets message author 
     *  @return {User} message author
     */
    Message.prototype.getOwner = function () {
        return this.owner;
    };

    /** Gets date of creation 
     *  @return {Date} date of creation
     */
    Message.prototype.getDateOfCreation = function () {
        return new Date(this.createdOn);
    };

    /** Gets text of the message 
     *  @return {string} text message
     */
    Message.prototype.getMessage = function () {
        return this.message;
    };

    Object.defineProperty(globalObject, "Message", {
        configurable: false,
        get: function () { return Message; },
        set: function () { throw new Error("Modifying of 'Message' is forbidden!"); }
    });


    /**
     * Represents a user.
     * @constructor
     * @param {string} userName - name of the user
     */
    var User = function (userName) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof userName !== "string", "userName argument is not a string!", TypeError);
        } catch (err) {
            console.error("Cannot create instance of User: " + err.message);
            throw err;
        }  

        this.name = userName;
        this.defaultChat;
    };

    /** Gets the user name 
     * @return {string} user name
     */
    User.prototype.getName = function () {
        return this.name;
    };

    /** Sets user's default chat 
     * @param {Chat} chat - instance of Chat
     */
    User.prototype.setDefaultChat = function (chat) {
        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
        } catch (err) {
            console.error("Cannot set default chat: " + err.message);
            throw err;
        }
        
        this.defaultChat = chat;
    };

    /** Connects user to the chat
     * @param {Chat} [chat] - chat to connect to
     */
    User.prototype.joinChat = function (chat) {
        if (arguments.length < 1) {
            this.defaultChat.join(this);
        } else {
            try {
                throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
            } catch (err) {
                console.error("Cannot connect to chat: " + err.message);
                throw err;
            }

            chat.join(this);
        }
    };

    /** Disconnects user from the chat
     * @param {Chat} [chat] - chat to disconnect from
     */    
    User.prototype.leaveChat = function (chat) {
        if (arguments.length < 1) {
            this.defaultChat.leave(this);
        } else {
            try {
                throwError(chat instanceof Chat === false, "Argument is not an instance of Chat!", TypeError);
            } catch (err) {
                console.error("Cannot disconnect from chat: " + err.message);
                throw err;
            }

            chat.leave(this);
        }
    };

    /** Sends the message to the chat
     * @param {Chat} [chat] - the chat to send to
     * @param {string} message - the text message to send
     */
    User.prototype.sendMessage = function (chat, textMessage) {
        if (arguments.length > 1) {
            chat = arguments[0];
            textMessage = arguments[1];
        } else if (arguments.length == 1) {
            textMessage = arguments[0];
            chat = this.defaultChat;
        }

        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof textMessage !== "string", "textMessage argument is not a string!", TypeError);
            throwError(chat instanceof Chat === false, "Chat argument is not an instance of Chat!", TypeError);
        } catch (err) {
            console.error("Cannot send message: " + err.message);
            throw err;
        }

        chat.sendMessage(this, textMessage);
    };

    /** 
     * Logs to console new message(s)
     * @param {Chat} chat - the chat to log messages from
     * @param {number} [amount=10] - amount of messages to log
     *//** 
     * Logs to console new message(s) from default chat
     * @param {number} amount - amount of messages to log
     */
    User.prototype.logNewMessages = function (chat, amount) {
        var defaultAmount = 10; 

        if (arguments.length == 1) {
            if (arguments[0] instanceof Chat) {
                chat = arguments[0];
                amount = defaultAmount;

            } else {
                amount = arguments[0];
                chat = this.defaultChat;
            }
        } else if (arguments.length == 2) {
            if (arguments[0] instanceof Chat) {
                chat = arguments[0];
                amount = arguments[1];

            } else {
                chat = arguments[1];
                amount = arguments[0];
            }
        }

        try {
            throwError(arguments.length < 1, "Not enough arguments!", ReferenceError);
            throwError(typeof amount !== "number", "Amount argument is not a number!", TypeError);
            throwError(chat instanceof Chat === false, "Chat argument is not an instance of Chat!", TypeError);
        } catch (err) {
            console.error("Cannot log new messages: " + err.message);
            throw err;
        }

        var messages = chat.getNewMessages(this, amount);

        for (var i = 0; i < messages.length; i++) {
            chat.logMessage(messages[i]);
        }
    };

    Object.defineProperty(globalObject, "User", {
        configurable: false,
        get: function () { return User; },
        set: function () { throw new Error("Modifying of 'User' is forbidden!"); }
    });

    /** Throws new error if condtition is true
     * @param {boolean} condition - condition
     * @param {string} message - error message
     * @param {Error | TypeError | ReferenceError} [errorType] - type of error object
     */
    function throwError (condition, message, errorType) {
        if (typeof globalObject.Message === "undefined") {
            errorType = Error;
        }

        if (condition) {
            throw new errorType(message);
        }
    }
})();
